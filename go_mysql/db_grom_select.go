package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

//model：
type TUser struct {
	ID        uint `gorm:"primaryKey"`
	Name      string
	Password  string
	Phone     string    `gorm:"phone"`
	NickName  string    `gorm:"nick_name"`
	CreatedAt time.Time `gorm:"column:created_at;type:datetime"`
	UpdatedAt time.Time `gorm:"column:updated_at;type:datetime"`
	DeletedAt time.Time `gorm:"column:deleted_at;type:datetime"`
}

//指定数据库表名称
func (TUser) TableName() string {
	return "t_user"
}

func main() {

	//启用打印日志
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // 慢 SQL 阈值
			LogLevel:      logger.Info, // Log level: Silent、Error、Warn、Info
			Colorful:      false,       // 禁用彩色打印
		},
	)
	dsn := "root:dark123456@tcp(120.78.238.75:3306)/go_admin?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	//获取第一条
	var user01 = TUser{}
	db.First(&user01)
	fmt.Printf("S: %#v\n", user01)

	// 获取一条记录，没有指定排序字段
	var user02 = TUser{}
	db.Take(&user02)
	fmt.Printf("S: %#v\n", user02)

	//获取最后一条
	var user03 = TUser{}
	db.Last(&user03)
	fmt.Println(user03)

	//获取第一条匹配的记录
	var user04 = TUser{}
	result01 := db.Where("name", "xx").First(&user04)
	fmt.Println(result01.RowsAffected)
	//或者这样写
	db.Find(&user04, "name", "xx")
	fmt.Println("或者这样写:", result01.RowsAffected)

	// 获取全部匹配的记录
	var user05 = TUser{}
	result02 := db.Where("name <> ?", "r").First(&user05)
	fmt.Println(result02.RowsAffected)

	//in
	var user06 = TUser{}
	db.Where("name IN ?", []string{"r", "r 2"}).Find(&user06)

	//like
	db.Where("name like ?", "%aa%").Find(&user06)

	//and
	db.Where("name =? and phone =?", "r", "12345").Find(&user06)

	//time
	db.Where("created_at >?", "2022-05-12").Find(&user06)

	// BETWEEN
	db.Where("created_at BETWEEN ? AND ?", "lastWeek", "today").Find(&user06)

	//Struct--查询，用model来匹配参数
	db.Where(&TUser{Name: "r", Phone: "123"}).Find(&user06)

	// Map ----查询，用json的方式来匹配参数
	db.Where(map[string]interface{}{"name": "r", "phone": "123"}).Find(&user06)

	//-------------------------------------------------------------------or---------------------------------
	db.Where("name", "r").Or("phone", "123").Find(&user06)

	db.Where("name", "r").Or(&TUser{Phone: "123", NickName: "x"}).Find(&user06)

	//------------------------------------------------------------------order--------------------------

	db.Order("id desc").Find(&user06)

	//多个order
	db.Order("id desc,created_at asc").Find(&user06)

	//---------------------------------------------------------------Limit & Offse--------------
	db.Limit(1).Find(&user06)
	db.Offset(1).Find(&user06)
	//分页操作
	var PageNum = 1
	var PageSize = 10
	db.Limit(PageSize).Offset((PageNum - 1) * PageSize).Find(&user06)

	//------------------------------------------------Count # ----------------------------

	var count int64
	db.Table("t_user").Count(&count)
	fmt.Print("条数：", count)

}
