module strom-huang-go/go_mysql

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	gorm.io/driver/mysql v1.3.3
	gorm.io/gorm v1.23.5
)
