package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

//model：
type TUser struct {
	ID        uint `gorm:"primaryKey"`
	Name      string
	Password  string
	Phone     string    `gorm:"phone"`
	NickName  string    `gorm:"nick_name"`
	CreatedAt time.Time `gorm:"column:created_at;type:datetime"`
	UpdatedAt time.Time `gorm:"column:updated_at;type:datetime"`
	DeletedAt time.Time `gorm:"column:deleted_at;type:datetime"`
}

//指定数据库表名称
func (TUser) TableName() string {
	return "t_user"
}

func main() {
	//启用打印日志
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // 慢 SQL 阈值
			LogLevel:      logger.Info, // Log level: Silent、Error、Warn、Info
			Colorful:      false,       // 禁用彩色打印
		},
	)
	dsn := "root:dark123456@tcp(120.78.238.75:3306)/go_admin?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	//普通创建
	user := TUser{Name: "test", Password: "123", NickName: "hello", Phone: "123", CreatedAt: time.Now()}
	result := db.Create(&user) // 通过数据的指针来创建
	fmt.Println(result.RowsAffected)

	//创建记录并更新给出的字段
	db.Select("name", "password", "phone", "nick_name").Create(&user)

	//创建记录并更新未给出的字段
	db.Omit("phone").Create(&user)

	//批量插入 使用 CreateInBatches 创建时，你还可以指定创建的数量
	var users = []TUser{{Name: "test1", Password: "123", NickName: "hello1", CreatedAt: time.Now()}, {Name: "test2", Password: "123", NickName: "hello2", CreatedAt: time.Now()}}
	db.Create(&users)

	db.CreateInBatches(&users, 100)

}
