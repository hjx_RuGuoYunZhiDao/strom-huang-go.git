package main

import (
	"fmt"
	"log"
	"os"
	model "strom-huang-go/go_mysql/model"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func main() {
	//启用打印日志
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // 慢 SQL 阈值
			LogLevel:      logger.Info, // Log level: Silent、Error、Warn、Info
			Colorful:      false,       // 禁用彩色打印
		},
	)
	dsn := "root:dark123456@tcp(120.78.238.75:3306)/go_admin?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	//删除一条 DELETE FROM `t_user` WHERE `t_user`.`id` = 3
	db.Delete(&model.TUser{ID: 3})

	//带额外参数 DELETE FROM `t_user` WHERE name = 'jinzhu' AND `t_user`.`id` = 3
	db.Where("name = ?", "jinzhu").Delete(&model.TUser{ID: 3})

	// 根据主键删除 DELETE FROM `t_user` WHERE `t_user`.`id` = 3
	db.Delete(&model.TUser{}, 3)

	//多个删除：相当于In : DELETE FROM `t_user` WHERE `t_user`.`id` IN (3,4,5)
	db.Delete(&model.TUser{}, []int{3, 4, 5})

	//批量删除 :  DELETE FROM `t_user` WHERE nick_name like
	db.Delete(&model.TUser{}, "nick_name like ?", "%6666%")

	// 和上面一个意思 DELETE FROM `t_user` WHERE phone like '%00000%'
	db.Where("phone like ?", "%00000%").Delete(&model.TUser{})

	//-------------------------------------软删除------------------------------------------------
	//软删除有点特殊，如果你的model里面有delete_at或者其他标识符号，可以在上面加上：gorm.deletedat 来表述
	//如果没有的话，可以用修改的方法：

	//UPDATE `t_user` SET `deleted_at`='2022-05-13 11:51:48.981' WHERE `t_user`.`id` = 3 AND `t_user`.`deleted_at` IS NULL
	db.Delete(&model.TUser{ID: 3})

	// 批量删除 UPDATE `t_user` SET `deleted_at`='2022-05-13 11:53:51.055' WHERE name = 20 AND `t_user`.`deleted_at` IS NULL
	db.Where("name = ?", 20).Delete(&model.TUser{})

	// 在查询时会忽略被软删除的记录 SELECT * FROM `t_user` WHERE age = 20 AND `t_user`.`deleted_at` IS NULL
	db.Where("age = 20").Find(&model.TUser{})
}
