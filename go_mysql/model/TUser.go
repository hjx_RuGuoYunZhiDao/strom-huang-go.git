package model

import (
	"time"

	"gorm.io/gorm"
)

//model：
type TUser struct {
	ID        uint `gorm:"primaryKey"` //表示主键
	Name      string
	Password  string
	Phone     string         `gorm:"phone"`
	NickName  string         `gorm:"nick_name"`
	CreatedAt time.Time      `gorm:"column:created_at;type:datetime"`
	UpdatedAt time.Time      `gorm:"column:updated_at;type:datetime"`
	DeletedAt gorm.DeletedAt //表示软删除的标识
}

//指定数据库表名称
func (TUser) TableName() string {
	return "t_user"
}
