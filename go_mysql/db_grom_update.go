package main

import (
	"fmt"
	"log"
	"os"
	model "strom-huang-go/go_mysql/model"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func main() {
	//启用打印日志
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // 慢 SQL 阈值
			LogLevel:      logger.Info, // Log level: Silent、Error、Warn、Info
			Colorful:      false,       // 禁用彩色打印
		},
	)
	dsn := "root:dark123456@tcp(120.78.238.75:3306)/go_admin?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	// 条件更新 UPDATE `t_user` SET `name`='aa',`updated_at`='2022-05-13 10:19:57.743' WHERE name = 'a'
	db.Model(&model.TUser{}).Where("name = ?", "a").Update("name", "aa")

	//根据ID更新 UPDATE `t_user` SET `name`='test',`updated_at`='2022-05-13 10:27:49.548' WHERE `id` = 1
	var user = model.TUser{ID: 1}
	db.Model(&user).Update("name", "test")

	//--------------------------------------更新多列---------------------------------------------
	//  根据 `struct` 更新属性，只会更新非零值的字段
	//  UPDATE `t_user` SET `id`=1,`name`='hello',`password`='1',`phone`='123',`updated_at`='2022-05-13 10:32:17.812'
	db.Model(&model.TUser{ID: 1}).Updates(model.TUser{Name: "hello", Password: "1", Phone: "123"})

	// 根据 `map` 更新属性
	// UPDATE `t_user` SET `password`='111',`phone`='456',`name`='hello',`updated_at`='2022-05-13 10:35:09.953' WHERE `id` = 1
	db.Model(&model.TUser{ID: 1}).Updates(map[string]interface{}{"name": "hello", "Password": "111", "Phone": "456"})

	//--------------------------------------更新选定字段--------------------------------------
	//忽略某些字段：UPDATE `t_user` SET `name`='hello',`updated_at`='2022-05-13 10:38:56.004' WHERE `id` = 1
	db.Model(&model.TUser{ID: 1}).Select("name").Updates(map[string]interface{}{"name": "hello", "Password": "111", "Phone": "456"})

	//onmit 不更新这个字段 UPDATE `t_user` SET `password`='111',`phone`='456',`updated_at`='2022-05-13 10:39:57.516' WHERE `id` = 1
	db.Model(&model.TUser{ID: 1}).Omit("name").Updates(map[string]interface{}{"name": "hello", "Password": "111", "Phone": "456"})

	//--------------------------------------批量更新--------------------------------------
	// 根据 struct 更新
	//UPDATE `t_user` SET `name`='hello',`created_at`='2022-05-13 10:45:11.233',`updated_at`='2022-05-13 10:45:11.255' WHERE phone = '122222'
	db.Model(&model.TUser{}).Where("phone = ?", "122222").Updates(&model.TUser{Name: "hello", CreatedAt: time.Now()})
	// 根据 map 更新 UPDATE `t_user` SET `name`='hello',`password`='1213' WHERE id IN (1,2)
	db.Table("t_user").Where("id IN ?", []int{1, 2}).Updates(map[string]interface{}{"name": "hello", "password": "1213"})
	//更新的记录数
	result := db.Model(model.TUser{}).Where("phone = ?", "122222").Updates(model.TUser{Name: "hello", Password: "1213"})
	fmt.Print(result.RowsAffected)
}
