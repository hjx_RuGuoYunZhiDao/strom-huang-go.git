package main

import (
	"fmt"
	config "strom-huang-go/go_redis/config"
)

func main() {
	err := config.InitRedisClient()
	if err != nil {
		//redis连接错误
		panic(err)
	}
	fmt.Println("Redis连接成功")

	//----------------------SAdd & SCard

	// 添加100到集合中
	err = config.RedisDb.SAdd("redis-test", 100).Err()

	// 将100,200,300批量添加到集合中 集合的元素不能重复
	config.RedisDb.SAdd("redis-test", 100, 200, 300)

	//获取集合中元素的个数
	size, err := config.RedisDb.SCard("redis-test").Result()
	fmt.Print(size)

	//-------------------SIsMember & SMembers示例
	//SIsMember判断集合中是否包含某个值
	flag, err := config.RedisDb.SIsMember("redis-test", "222200").Result()
	fmt.Print(flag)

	//SMembers 返回集合中所有的值
	rts, err := config.RedisDb.SMembers("redis-test").Result()
	fmt.Print(rts)

	//-----------------------并集 SInterStore &交集 SInter &差集
	config.RedisDb.SAdd("big-key", "a", "b", "c")
	config.RedisDb.SAdd("smal-key", "a", "b", "d")
	//交集：既在big-key中，又在smal-key中
	names, err := config.RedisDb.SInter("big-key", "smal-key").Result()
	fmt.Print("交集结果是:", names)

	//求交集并将交集保存到 destSet 的集合
	des, err := config.RedisDb.SInterStore("destSet", "big-key", "smal-key").Result()
	fmt.Print("交集结果处理成功条数是:", des)
	destset, err := config.RedisDb.SMembers("destSet").Result()
	fmt.Print("destSet:", destset)

	//差集
	str, err := config.RedisDb.SDiff("big-key", "smal-key").Result()
	fmt.Print("差集:", str)

	//----------------------- 集合删除操作
	err = config.RedisDb.SAdd("redis-test", 1, 2, 3, 4, 5, 6, 7, 8, 9).Err()
	// 随机返回集合中的一个元素，并且删除这个元素
	member1, err := config.RedisDb.SPop("redis-test").Result()
	fmt.Println(member1)

	res2, _ := config.RedisDb.SMembers("redis-test").Result()
	fmt.Println(res2)

	// 随机返回集合中的4个元素，并且删除这些元素
	member2, err := config.RedisDb.SPopN("redis-test", 3).Result()
	fmt.Println(member2)
	res23, _ := config.RedisDb.SMembers("redis-test").Result()
	fmt.Println(res23)
	// 删除集合 名称为300,400的元素,并返回删除的元素个数
	member3, err := config.RedisDb.SRem("redis-test", 1, 2, 3).Result()
	fmt.Println(member3)
	res3, _ := config.RedisDb.SMembers("redis-test").Result()
	fmt.Println(res3)

	//----------------随机数
	//返回几个中随机任意一个
	str1, err := config.RedisDb.SRandMember("redis-test").Result()
	fmt.Println(str1)

	//返回几个中随机任意3个
	str2, err := config.RedisDb.SRandMemberN("redis-test", 3).Result()
	fmt.Println(str2)

	//--------------------------SMembersMap & SMove示例
	//把集合里的元素转换成map的key
	map1, err := config.RedisDb.SMembersMap("redis-test").Result()
	fmt.Println(map1)

	//新增一个key
	config.RedisDb.SAdd("redis-test2", "yyyy")

	//把redis-test 集合中4 移动到 redis-test2中
	ok, err := config.RedisDb.SMove("redis-test", "redis-test2", 4).Result()

	//如果没有4就是false
	fmt.Println(ok)

	str5, err := config.RedisDb.SMembers("redis-test2").Result()
	//[4 yyyy]
	fmt.Println(str5)

}
