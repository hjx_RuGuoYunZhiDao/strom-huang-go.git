package main

import (
	"fmt"
	config "strom-huang-go/go_redis/config"
)

func main() {
	err := config.InitRedisClient()
	if err != nil {
		//redis连接错误
		panic(err)
	}
	fmt.Println("Redis连接成功")

	// -------------------------list-操作 -----------------------------------
	//LPushX & LPush

	//LPush ： 此时列表不存在，依然可以插入
	n, err := config.RedisDb.LPush("testList", "tonoy1").Result()
	fmt.Println("number:", n)

	err = config.RedisDb.LPush("testList", "tonoy1", "tonoy3", "tonoy2").Err()
	if err != nil {
		//redis连接错误
		panic(err)
	}

	//LPushX：仅当K存在的时候才插入数据,此时列表不存在，无法插入
	t, err := config.RedisDb.LPushX("testList1", "aaa").Result()
	fmt.Println("number:", t)

	// LRange & LLen
	// 返回从0开始到-1位置之间的数据，意思就是返回全部数据
	vals, err := config.RedisDb.LRange("testList", 0, -1).Result()
	if err != nil {
		panic(err)
	}
	fmt.Println(vals)

	// 返回从0开始到2 位置之间的数据，意思就是返回全部数据
	vals2, err := config.RedisDb.LRange("testList", 0, 2).Result()
	if err != nil {
		panic(err)
	}
	fmt.Println(vals2)

	//返回list集合中的长度
	testList, err := config.RedisDb.LLen("testList").Result()
	if err != nil {
		panic(err)
	}

	fmt.Println("testList集合的长度为:", testList)

	// LTrim & LIndex
	// 截取（可以理解为删除）名称为testList 的 key,并把截取后的值赋值给studentList
	val := config.RedisDb.LTrim("testList", 0, 3)
	fmt.Println(val)

	//返回 testList list中第三个值
	index, err := config.RedisDb.LIndex("testList", 2).Result()
	fmt.Println(index)

	//LSet & LInsert
	//替换 testList 中第三条数据
	bee, err := config.RedisDb.LSet("testList", 2, "bee").Result()
	fmt.Print(bee)

	//在list列表testList 中值为bee 前面添加元素hello ： 第二个参数有：after ，前面/后面
	config.RedisDb.LInsert("testList", "before", "bee", "hello")
	//config.RedisDb.LInsertBefore("studentList","lilei","hello") 执行效果一样

	config.RedisDb.LInsert("testList", "after", "bee", "a")
	//config.RedisDb.LInsertBefore("studentList","lilei","hello") 执行效果一样

	//LPop & LRem示例
	//从列表左边删除第一个数据，并返回删除的数据 可以理解为删除最后一个数据
	config.RedisDb.LPop("testList")

	//删除列表中的数据。删除10个值为bee的数据重复元素
	config.RedisDb.LRem("testList", 10, "bee")

}
