package main

import (
	"fmt"
	config "strom-huang-go/go_redis/config"
)

func main() {

	err := config.initRedisClient()
	if err != nil {
		//redis连接错误
		panic(err)
	}
	fmt.Println("Redis连接成功")

	// ----------------------------------get/set------------------------
	var key string = "test-key"
	var keyValue string = "test"
	err = config.RedisDb.Set(key, keyValue, 0).Err()
	if err != nil {
		panic(err)
	}

	var value string
	value, err = config.RedisDb.Get("test-key").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("key:", value)

	//------------------getSet / setNx---------------------------------------
	var oldValue string
	oldValue, err = config.RedisDb.GetSet(key, "test").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println(oldValue)

	//如果key不存在，则设置这个key的值,并设置key的失效时间。如果key存在，则设置不生效
	err = config.RedisDb.SetNX(key, "test", 0).Err()
	if err != nil {
		panic(err)
	}

	//------------------MSet / MGet---------------------------------------
	//批量设置多个key-value值
	err = config.RedisDb.MSet("key1", "value1", "key2", "value2").Err()
	if err != nil {
		panic(err)
	}

	// MGet函数可以传入任意个key，一次性返回多个值。
	// 这里Result返回两个值，第一个值是一个数组，第二个值是错误信息
	values, err := config.RedisDb.MGet("key1", "key2", "key3").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println(values)

	//------------------GetRange():字符串截取 /Incr():增加+1 / IncrBy():按指定步长增加 /Decr  自减 /DecrBy 指定步数递减---------------------------------------
	//GetRange() : key,start-从那个位置开始截取，endt-从那个位置结束
	val, err := config.RedisDb.GetRange("test-key", 0, 4).Result()
	if err != nil {
		panic(err)
	}
	fmt.Println(val)

	///Incr()设置一个age测试自增、自减
	err = config.RedisDb.Set("number", "0", 0).Err()
	if err != nil {
		panic(err)
	}
	config.RedisDb.Incr("number") // 自增

	config.RedisDb.IncrBy("number", 10) // 自增 +10
	val, err = config.RedisDb.Get("number").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println(val)

	config.RedisDb.Decr("number")      // 自减
	config.RedisDb.DecrBy("number", 3) //-3 此时age的值是22

	val, err = config.RedisDb.Get("number").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println(val)

	//------------------ Append():追加 /StrLen():获取长度---------------------------------------
	//会在key-value后面增加 '你好'
	length, err := config.RedisDb.Append("test-key", "你好").Result()
	if err != nil {
		panic(err)
	}
	fmt.Print("当前缓存key的长度", length)

	var result string
	result, err = config.RedisDb.Get("test-key").Result()
	fmt.Print(result)

	num, err := config.RedisDb.StrLen("test-key").Result()
	if err != nil {
		panic(err)
	}
	fmt.Printf("当前缓存key的长度为: %v\n", num)

	//------------------ Del():删除 /Expire:设置过期时间---------------------------------------
	var expire bool
	expire, err = config.RedisDb.Expire("test-key", 100).Result()
	fmt.Print(expire)

	var length2 int64
	length2, err = config.RedisDb.Del("test-key").Result()
	fmt.Print(length2)

}
