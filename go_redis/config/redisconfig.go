package config

import (
	"github.com/go-redis/redis"
)

// 声明一个全局的redisDb变量
var RedisDb *redis.Client

func InitRedisClient() (err error) {
	RedisDb = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       0,
	})
	_, err = RedisDb.Ping().Result()
	if err != nil {
		return err
	}
	return nil
}
