<a name="og1mf"></a>
## 1、mencached 介绍（一下简称mem）
> Memcached是一个自由开源的，高性能，分布式内存对象缓存系统。
> 基于内存的key-value存储，用来存储小块的任意数据（字符串、对象）。这些数据可以是数据库调用、API调用或者是页面渲染。
> 一般的使用目的是，通过缓存数据库查询结果，减少数据库访问次数，以提高动态Web应用的速度、提高可扩展性
> Memcached 官网：[https://memcached.org/](https://memcached.org/)
> github地址 ：[https://github.com/memcached/memcached/wiki/Install](https://github.com/memcached/memcached/wiki/Install)



<a name="PlMBY"></a>
## 2、使用mem之前需要先安装服务端
> windows安装服务端的话去搜一下，我测试用的时候是unbantu的
> 

<a name="HrGal"></a>
### 2.1 unbantu安装mem服务端
```
1/本地包索引
sudo apt update

2/安装官方包
sudo apt install memcached

3/安装libmemcached-tools ，这是一个提供多种工具来与Memcached服务器配合使用的库
sudo apt install libmemcached-tools

4/这个时候应该是自己启动了，可以用命令查看一下
memcached

或者

service memcached status

或者

lsof -i :11211


5/查看端口是否存在
netstat -ntlp
----
tcp        0      0 0.0.0.0:11211           0.0.0.0:*               LISTEN      18846/memcached 



6/重启和停止使用命令
service memcached start

service memcached restart

service memcached stop

```
<a name="b9jMQ"></a>
### 2.2 unbantu修改mem配置文件
```
1/ 使用 which memcached 查看安装目录：一般都在 /usr/bin/memcached

2/ 配置文件目录在： /etc/

vim  /etc/memcached.conf

要检查接口设置，请在文件中找到以下行：
-l 127.0.0.1

```
![image.png](https://cdn.nlark.com/yuque/0/2022/png/21690081/1658892432769-5d899cfb-9622-470f-878a-76e6d7ec17e9.png#clientId=u2b69ef57-f1b3-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=505&id=ud8e86220&margin=%5Bobject%20Object%5D&name=image.png&originHeight=505&originWidth=950&originalType=binary&ratio=1&rotation=0&showTitle=false&size=33910&status=done&style=none&taskId=u8e8ddb7f-8b06-4b24-aae2-2272604b843&title=&width=950)




<a name="blJT6"></a>
## 3、Go 整合mem (相当于前面的准备是服务端，现在需要使用它了)
> 我使用的 gin 来测试缓存的，大部分都一样，有些包就自己下载，也可以看我之前写的gin整合

<a name="kDhDn"></a>
### 3.1、目录结构
```
go-cashe
├─ main.go
├─ memcached
│  └─ mem.go
└─ README.md
```

<a name="k3H2S"></a>
### 3.2、主要代码：mem.go
> 其中常用的代码我都写上去了，不满足需求的话可以自己去网上搜索一下

 

<a name="ghIwX"></a>
### 3.3、主要代码：main.go
 
<a name="WZXZ3"></a>
## 4、简单测试结果
> go run main.go 后控制台输出
> 浏览器输入：
> [http://127.0.0.1:8080/memCashe/123](http://127.0.0.1:8080/memCashe/123)


![image.png](https://cdn.nlark.com/yuque/0/2022/png/21690081/1658893634787-705bf4a7-2365-4272-b43d-afb6146104de.png#clientId=u2b69ef57-f1b3-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=113&id=ud785e71e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=113&originWidth=999&originalType=binary&ratio=1&rotation=0&showTitle=false&size=11500&status=done&style=none&taskId=u9eba8d35-7178-405b-a8b1-95e961cadc5&title=&width=999)


<a name="OV1yV"></a>
## 5、补充一个小知识
> go run -race main.go
> _-race 參數是 go 的 _[Race Detector](https://blog.golang.org/race-detector)_，內建整合工具，可以輕鬆檢查出是否有 race condition_
 
