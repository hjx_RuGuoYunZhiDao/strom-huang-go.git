
```
go-cashe
├─ .vscode
│  └─ launch.json
├─ freecache
│  └─ free.go
├─ go.mod
├─ go.sum
├─ main.go
├─ memcached
│  └─ mem.go
├─ README-free.md
└─ README-mem.md

```

### 本地缓存有些库
freecache
bigcache
fastcache
go-cache

### 分布式缓存库
redis
memcached