package main

import (
	"fmt"
	"net/http"
	free "strom-huang-go/go-cashe/freecache"
	"strom-huang-go/go-cashe/memcached"

	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()
	//测试memcache缓存
	r.GET("/memCashe/:uid", func(c *gin.Context) {
		uid := c.Param("uid")

		s := memcached.GetMemCasheValueString(uid)
		if len(s) > 0 {
			fmt.Println("缓存有值---" + s)
		} else {
			memcached.SetMemCasheKey(uid, "hello")
			s = "hello"
		}
		c.JSON(http.StatusOK, gin.H{
			"message": s,
		})
	})

	//测试free缓存
	r.GET("/free/:uid", func(c *gin.Context) {
		uid := c.Param("uid")

		fMod := free.FreeCasheModel{
			Key:           []byte(uid),
			Value:         []byte("fee---测试"),
			ExpireSeconds: 100,
		}

		_, b := free.FreeGet(fMod)
		if b {
			dBool := free.FreeCasheDel(fMod)
			if dBool {
				fmt.Println("删除了到了缓存值---")
			}
		} else {
			sBool := free.FreeCacheSet(fMod)
			if sBool {
				fmt.Println("插入到了缓存值---")
			}
		}
		c.JSON(http.StatusOK, gin.H{
			"message": b,
		})
	})

	r.Run("127.0.0.1:8080")
}
