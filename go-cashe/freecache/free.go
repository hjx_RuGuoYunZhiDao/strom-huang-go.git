package freecache

import (
	"github.com/coocood/freecache"
)

//初始化Cache
// freecache.NewCacheCustomTimer(100*1024*1024, freecache.NewCachedTimer()) 这种方式也可以
var cache = freecache.NewCache(100 * 1024 * 1024)

//定一个 model struct
type FreeCasheModel struct {
	Key           []byte
	Value         []byte
	ExpireSeconds int //过期时间-s
}

//Get
func FreeGet(free FreeCasheModel) (string, bool) {
	value, err := cache.Get(free.Key)
	if err != nil {
		return "", false
	}
	return string(value), true
}

//GetOrSet
//如果没有就存入新的key
func FreeCacheGetOrSet(free FreeCasheModel) (string, bool) {
	retValue, err := cache.GetOrSet(free.Key, free.Value, free.ExpireSeconds)
	if err != nil {
		return "", false
	}
	return string(retValue), true
}

//Set
func FreeCacheSet(free FreeCasheModel) bool {
	err := cache.Set(free.Key, free.Value, free.ExpireSeconds)
	if err != nil {
		return false
	}
	return true
}

//SetAndGet
func FreeCacheSetAndGet(free FreeCasheModel) (string, bool) {
	retValue, found, err := cache.SetAndGet(free.Key, free.Value, free.ExpireSeconds)
	if err != nil {
		return "", false
	}
	return string(retValue), found
}

//更新key的过期时间--如果这个key不存在会返回错误
func FreeChasheTouch(free FreeCasheModel) bool {
	err := cache.Touch(free.Key, free.ExpireSeconds)
	if err != nil {
		return false
	}
	return true
}

//删除---
func FreeCasheDel(free FreeCasheModel) bool {
	return cache.Del(free.Key)
}
