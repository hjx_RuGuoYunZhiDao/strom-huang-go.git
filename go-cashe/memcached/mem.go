package memcached

import (
	"fmt"

	"github.com/bradfitz/gomemcache/memcache"
)

//这里可以写多个，用逗号分隔
var mc = memcache.New("192.168.0.250:11211")

//set 和add 方法的区别
// 如果要设置的key不存在时，则set方法与add方法的效果一致
// 如果要设置的key已经存在时，则set方法与replace方法效果一样

//增加缓存 Set方法
func SetMemCasheKey(key string, value string) bool {
	s := &memcache.Item{
		Key:        key,
		Value:      []byte(value),
		Flags:      0,   //应该是多个服务器地址是否能相互获取的意思
		Expiration: 100, //过期时间 秒为单位
	}
	err := mc.Set(s)
	if err != nil {
		fmt.Printf("错误： %s", err.Error())
		return false
	}
	return true
}

//增加缓存 add方法
func SetMemCasheAdd(key string, value string) bool {
	s := &memcache.Item{
		Key:        key,
		Value:      []byte(value),
		Flags:      0,   //应该是多个服务器地址是否能相互获取的意思
		Expiration: 100, //过期时间
	}
	err := mc.Add(s)
	if err != nil {
		return false
	}
	return true
}

//获取Key
func GetMemCasheValueString(key string) string {
	item, err := mc.Get(key)
	if err != nil {
		return ""
	}
	fmt.Println("获取的key :" + item.Key)
	return string(item.Value)
}

//替换缓存的值
func ReplaceMemCasheValue(key string, value string) bool {
	s := &memcache.Item{
		Key:        key,
		Value:      []byte(value),
		Flags:      0,
		Expiration: 100,
	}
	err := mc.Replace(s)
	if err != nil {
		return false
	}
	return true
}

//删除key
func DeleteMemCasheValue(key string) bool {

	if mc.Delete(key) != nil {
		return false
	}
	return true
}

//删除全部
func DeleteMemCasheAll() {
	mc.DeleteAll()
}
