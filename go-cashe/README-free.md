## 1、freecache 介绍(以下简称free)
> 用于 Go 的缓存库，具有零 GC 开销和高并发性能, 厉不厉害？


## 2、 free 获取
> git官网： [https://github.com/coocood/freecache](https://github.com/coocood/freecache)
> golang 获取：go get -u github.com/coocood/freecache
> 个人觉得比较好的文档：（不得不说大佬就是写得好）
>  [https://juejin.cn/post/7072121084136882183](https://juejin.cn/post/7072121084136882183) 
>  [https://cdmana.com/2022/03/202203071328243883.html](https://cdmana.com/2022/03/202203071328243883.html) 


## 3、golang 整合 free
```
```
go-cashe
├─ .vscode
│  └─ launch.json
├─ freecache
│  └─ free.go
├─ main.go
├─ README-free.md
```
```
### 3.1、主要代码 （free.go）
```
```


## 4、简单测试
![image.png](https://cdn.nlark.com/yuque/0/2022/png/21690081/1658909131210-28299181-d4cf-4f99-8dca-d9c1f37c1307.png#clientId=u8d005e8c-f66e-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=136&id=u0dfd42a6&margin=%5Bobject%20Object%5D&name=image.png&originHeight=136&originWidth=954&originalType=binary&ratio=1&rotation=0&showTitle=false&size=16195&status=done&style=none&taskId=u390ca783-09a2-47c3-98c2-78ae6295d1d&title=&width=954)
## 5、补充说明
> 1、代码看起来很简单，但是要多看源码多看文档是怎么实现的
> 2、 如果要测试性能问题老师们可以自己去测试，这里只是简单的实现一下
> 3、 上篇文档我写了memcached的缓存，我觉得这个更简单一些，但是应用场景是不一样的， memcached主要是分布式循环，多个项目可以同时去访问，free更适合单项目，并发高访问量多的。

## 6、文档代码地址
> [https://gitee.com/hjx_RuGuoYunZhiDao/strom-huang-go.git](https://gitee.com/hjx_RuGuoYunZhiDao/strom-huang-go.git)   ---[go-cashe](https://gitee.com/hjx_RuGuoYunZhiDao/strom-huang-go/tree/master/go-cashe)目录

