package main

import (
	"fmt"
	"strom-huang-go/go_zap/common/log"

	"github.com/spf13/viper"
)

func main() {
	VipInit()

	//初始化日志
	log.InitLog()

	log.GetLogger().Info("成功")
}

func VipInit() {
	// viper.SetConfigName("config1") // 读取yaml配置文件
	viper.SetConfigName("setting") // 读取json配置文件
	//viper.AddConfigPath("/etc/appname/")   //设置配置文件的搜索目录
	//viper.AddConfigPath("$HOME/.appname")  // 设置配置文件的搜索目录
	viper.AddConfigPath(".") // 设置配置文件和可执行二进制文件在用一个目录
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			fmt.Println("no such config file")
		} else {
			// Config file was found but another error was produced
			fmt.Println("read config error")
		}
		// 读取配置文件失败致命错误
		fmt.Errorf(err.Error())
	}
}
