module strom-huang-go/go_kafka

go 1.16

require (
	github.com/Shopify/sarama v1.33.0
	github.com/bsm/sarama-cluster v2.1.15+incompatible
	github.com/golang/glog v1.0.0
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
)
