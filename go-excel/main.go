package main

import (
	"fmt"

	"strom-huang-go/go-excel/app/service"
	config "strom-huang-go/go-excel/common"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func main() {
	VipInit()
	//初始化数据库
	config.MysqlSetup()
	r := gin.Default()
	//测试memcache缓存
	r.GET("/getUserExcel", service.GetUserExcelByMap)

	defer func() { // 必须要先声明defer，否则不能捕获到panic异常
		if err := recover(); err != nil {
			fmt.Println(err) // 这里的err其实就是panic传入的内容，55
		}
		fmt.Println("d")
	}()

	r.Run("127.0.0.1:8080")
}

func VipInit() {
	// viper.SetConfigName("config1") // 读取yaml配置文件
	viper.SetConfigName("setting") // 读取json配置文件
	//viper.AddConfigPath("/etc/appname/")   //设置配置文件的搜索目录
	//viper.AddConfigPath("$HOME/.appname")  // 设置配置文件的搜索目录
	viper.AddConfigPath(".") // 设置配置文件和可执行二进制文件在用一个目录
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			panic("Config file not found")
		} else {
			fmt.Println("read config error")
			panic("Config file was found but another error was produced")
		}
	}
}
