package model

import "time"

type TUser struct {
	ID         int64     `gorm:"column:id" db:"id" json:"id" form:"id"`
	Username   string    `gorm:"column:username" db:"username" json:"username" form:"username"` //  用户名
	Password   string    `gorm:"column:password" db:"password" json:"password" form:"password"` //  密码
	Remark     string    `gorm:"column:remark" db:"remark" json:"remark" form:"remark"`         //  备注
	Uuid       string    `gorm:"column:uuid" db:"uuid" json:"uuid" form:"uuid"`                 //  uuid
	Status     int64     `gorm:"column:status" db:"status" json:"status" form:"status"`         //  是否启用：0启用，1禁用
	Createtime time.Time `gorm:"column:createtime" db:"createtime" json:"createtime" form:"createtime"`
	Updatetime time.Time `gorm:"column:updatetime" db:"updatetime" json:"updatetime" form:"updatetime"`
}

func (TUser) TabName() string {
	return "t_user"
}

func UserprefixZn(column string) string {
	var res = "未知"
	switch column {
	case "id":
		res = "主键"
	case "username":
		res = "用户名"
	case "password":
		res = "***"
	case "remark":
		res = "备注"
	default:
		res = ""
	}
	return res
}
