module strom-huang-go/go_web_gin

go 1.16

require (
	github.com/gin-gonic/gin v1.7.7
	gorm.io/driver/mysql v1.3.3
	gorm.io/gorm v1.23.5
)
