package main

import (
	"fmt"
	"strom-huang-go/go_web_gin/config"
	Main "strom-huang-go/go_web_gin/router"
)

//启动路由--
func main() {
	fmt.Print("项目启动")
	//初始化数据库
	config.MysqlInit()
	Main.InitRouter()
}
