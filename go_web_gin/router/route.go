package router

import (
	"os"
	"path"
	api "strom-huang-go/go_web_gin/service"

	"github.com/gin-gonic/gin"
)

func InitRouter() {
	gin.SetMode(gin.ReleaseMode)
	//使用gin的Default方法创建一个路由handler
	router := gin.Default()
	//设置默认路由当访问一个错误网站时返回
	router.NoRoute(api.NotFound)
	//使用以下gin提供的Group函数为不同的API进行分组
	v1 := router.Group("user")
	{
		v1.GET("/login", api.Login)
	}

	router.GET("/", func(c *gin.Context) {
		path.Join()
		b, _ := os.ReadFile("D:\\my_work\\A6_My_go\\src\\strom-huang-go\\go_web_gin\\web3_clientVersion.html")

		c.Writer.Write(b)

	})

	//监听服务器端口
	router.Run(":8080")
}
