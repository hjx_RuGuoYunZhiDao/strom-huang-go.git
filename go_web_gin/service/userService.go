package service

import (
	"net/http"
	db "strom-huang-go/go_web_gin/config"
	model "strom-huang-go/go_web_gin/model"
	"strom-huang-go/go_web_gin/result"

	"github.com/gin-gonic/gin"
)

//登录
func Login(c *gin.Context) {
	//获取用户名和密码
	name := c.Request.FormValue("name")
	password := c.Request.FormValue("password")
	//先判断是否存在用户
	Bool := IsExistUser(name)
	var r = &result.R{}
	if Bool {
		//核对密码
		b := IsExistPwd(name, password)
		if !b {
			r.Code = 500
			r.Msg = "用户不存在"
		} else {
			r.Code = 200
			r.Msg = "欢迎"
		}
	} else {
		r.Code = 500
		r.Msg = "用户不存在"
	}

	c.JSON(http.StatusOK, r)
}

//判断用户是否存在 返回bool类型
func IsExistUser(user string) bool {
	err := db.MysqlDb.Where("name = ?", user).Find(&model.TUser{})
	return err == nil
}

//判断用户是否存在 返回bool类型
func IsExistPwd(name string, pwd string) bool {
	var user = &model.TUser{}
	d := db.MysqlDb.Where("name = ? ", name).Find(&user)
	if d.Error != nil {
		return false
	}
	if user.Password != pwd {
		return false
	}
	return true
}

//设置默认路由当访问一个错误网站时返回
func NotFound(c *gin.Context) {
	c.JSON(http.StatusNotFound, gin.H{
		"Code": 404, "Msg": "接口不存在",
	})
}
